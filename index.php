<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>WooCommerce JSON Helper</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
    .indent {
      margin-left: 2em;
    }

    .form-group {
      position: relative;
      border: 1px solid grey;
      padding: 2em;
    }
    .form-group .btn-secondary {
      position: absolute;
      top: 2px;
      right: 2px;
    }
    .repeatable {
      background-color: rgba(0,0,0,0.2);
      padding: 1em;
      margin: 1em 0;
    }
    .json {
      background: #2a2827;
      color: #d2c076;
      padding: 1em;
    }
    </style>
  </head>
  <body>
    <?php if(isset($_POST['available_finishes'])): ?>
    <h2>Available Finishes</h2>
    <pre class="json"><?php echo json_encode($_POST['available_finishes'], JSON_PRETTY_PRINT) ?></pre>
    <?php endif; ?>
    <?php if(isset($_POST['available_sizes'])): ?>
    <h2>Available Sizes</h2>
    <pre class="json"><?php echo json_encode($_POST['available_sizes'], JSON_PRETTY_PRINT) ?></pre>
    <?php endif; ?>
    <div class="container my-5">
      <form method="post">
        <h2>Available Finishes</h2>
        <?php if(isset($_POST['available_finishes'])): ?>
        <?php foreach( $_POST['available_finishes'] as $i => $group ): ?>
          <div class="form-group">
            <button type="button" class="btn btn-secondary btn-sm" data-repeat>+</button>
            <div class="repeatable" data-repeat-index="<?= $i ?>">
              <input type="text" class="mb-4 form-control" name="available_finishes[<?= $i ?>][title]" placeholder="Title" value="<?= $group['title'] ?>"/>
              <input type="text" class="mb-4 form-control" name="available_finishes[<?= $i ?>][fineprint]" placeholder="Fineprint" value="<?= $group['fineprint'] ?>"/>
              <div class="indent">
                <h3>Finishes</h3>
                <div class="form-group">
                  <button type="button" class="btn btn-secondary btn-sm" data-repeat>+</button>
                  <?php
                  foreach( array_values($group['finishes']) as $j => $finish ):
                  ?>
                  <div class="repeatable" data-repeat-index="<?= $j ?>">
                    <input type="text" class="mb-4 form-control" name="available_finishes[<?= $i ?>][finishes][<?= $j ?>][name]" placeholder="Name" value="<?= $finish['name'] ?>"/>
                    <input type="text" class="mb-4 form-control" name="available_finishes[<?= $i ?>][finishes][<?= $j ?>][description]" placeholder="Description" value="<?= $finish['description'] ?>"/>
                    <input type="text" class="form-control" name="available_finishes[<?= $i ?>][finishes][<?= $j ?>][image]" placeholder="Image" value="<?= $finish['image'] ?>"/>
                  </div>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
        <?php else: ?>
        <div class="form-group">
          <button type="button" class="btn btn-secondary btn-sm" data-repeat>+</button>
          <div class="repeatable" data-repeat-index="0">
            <input type="text" class="mb-4 form-control" name="available_finishes[0][title]" placeholder="Title"/>
            <input type="text" class="mb-4 form-control" name="available_finishes[0][fineprint]" placeholder="Fineprint"/>
            <div class="indent">
              <h3>Finishes</h3>
              <div class="form-group">
                <button type="button" class="btn btn-secondary btn-sm" data-repeat>+</button>
                <div class="repeatable" data-repeat-index="0">
                  <input type="text" class="mb-4 form-control" name="available_finishes[0][finishes][0][name]" placeholder="Name"/>
                  <input type="text" class="mb-4 form-control" name="available_finishes[0][finishes][0][description]" placeholder="Description"/>
                  <input type="text" class="form-control" name="available_finishes[0][finishes][0][image]" placeholder="Image"/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php endif; ?>
    <?php if(isset($_POST['available_finishes'])): ?>
    <pre class="json"><?php echo json_encode($_POST['available_finishes'], JSON_PRETTY_PRINT) ?></pre>
    <?php endif; ?>
    <div class="container my-5">
      <form method="post">
        <h2>Available Sizes</h2>
        <?php if(isset($_POST['available_sizes'])): ?>
        <?php foreach( $_POST['available_sizes'] as $i => $group ): ?>
          <div class="form-group">
            <button type="button" class="btn btn-secondary btn-sm" data-repeat>+</button>
            <div class="repeatable" data-repeat-index="<?= $i ?>">
              <input type="text" class="mb-4 form-control" name="available_sizes[<?= $i ?>][name]" placeholder="Name" value="<?= $group['name'] ?>"/>
              <input type="text" class="mb-4 form-control" name="available_sizes[<?= $i ?>][description]" placeholder="Description" value="<?= $group['description'] ?>"/>
              <input type="text" class="mb-4 form-control" name="available_sizes[<?= $i ?>][image]" placeholder="Image" value="<?= $group['image'] ?>"/>
            </div>
          </div>
        <?php endforeach; ?>
        <?php else: ?>
        <div class="form-group">
          <button type="button" class="btn btn-secondary btn-sm" data-repeat>+</button>
          <div class="repeatable" data-repeat-index="0">
            <input type="text" class="mb-4 form-control" name="available_sizes[0][name]" placeholder="Name"/>
            <input type="text" class="mb-4 form-control" name="available_sizes[0][description]" placeholder="Description"/>
            <input type="text" class="mb-4 form-control" name="available_sizes[0][image]" placeholder="Image"/>
          </div>
        </div>
        <?php endif; ?>
        <div class="mt-4">
          <button type="submit" class="btn btn-primary">Generate JSON</button>
        </div>
      </form>

    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        $("[data-repeat]").on("click", function(){
          var $parent = $(this).closest(".form-group");
          var $group = $parent.find("> [data-repeat-index]:last-child")
          var groupIndex = $group.data('repeat-index');
          var newGroupIndex = groupIndex + 1;
          var $newGroup = $group.clone();

          $newGroup.attr('data-repeat-index', newGroupIndex);
          $newGroup.find("input").each(function(){
            var inputName = $(this).attr('name');
            var oldIndex = '[' + groupIndex + ']';
            var newIndex = '[' + newGroupIndex + ']';
            var n = inputName.lastIndexOf(oldIndex);
            newInputName = inputName.slice(0, n) + inputName.slice(n).replace(oldIndex, newIndex);
            $(this).attr('name', newInputName);
          });

          $newGroup.appendTo($parent);
        });
    </script>
  </body>
</html>
